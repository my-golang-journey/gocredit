package routers

import (
	"gocredit/handler"

	"github.com/gin-gonic/gin"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "*")
		// c.Header("Access-Control-Allow-Headers", "*")1
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Accept, Origin, Cache-Control, X-Requested-With,Access-Control-Allow-Origin,Access-Control-Allow-Headers")
		// c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func PaymentRouter() {
	router := gin.Default()
	router.Use(CORSMiddleware())
	// config := cors.DefaultConfig()
	// config.AllowOrigins = []string{"http://127.0.0.1"}
	// config.AllowOrigins = []string{"http://google.com", "http://facebook.com"}
	// config.AllowAllOrigins = true

	// router.Use(cors.New(config))

	v1 := router.Group("/v1")
	v1.GET("/", handler.RootHandler)
	v1.GET("/:user_id/:id", handler.GetByIDHandler)
	v1.POST("/init", handler.InitHandler)
	v1.POST("/", handler.StoreHandler)
	v1.DELETE("/:id", handler.DeleteHandler)
	go router.Run()
}
