package handler

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"gocredit/libs"
	"gocredit/models"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
)

func RootHandler(c *gin.Context) {
	errEnv := godotenv.Load("env.local")
	if errEnv != nil {
		log.Fatalf("Some error occured. Err: %s", errEnv)
	}
	baseURL := os.Getenv("BASE_URL")
	start := time.Now()
	db := libs.DB()
	type Infos struct {
		GET     string
		GETBYID string
		POST    string
		DELETE  string
		INIT    string
	}

	inf := Infos{GET: baseURL, GETBYID: baseURL + ":user_id/:id", POST: baseURL, DELETE: baseURL + ":id", INIT: baseURL + "init"}

	var payments []models.Payment
	db.Find(&payments)
	elapsed := time.Since(start)

	c.JSON(http.StatusOK, gin.H{
		"message": "Data Loaded",
		"info":    inf,
		"time":    elapsed.String(),
		"data":    payments,
	})
}

func GetByIDHandler(c *gin.Context) {
	db := libs.DB()
	id := c.Param("id")
	userID := c.Param("user_id")
	var payment []models.Payment
	// err := db.Find(&payment, id).Error
	err := db.Where("user_id = ?", userID).Where("id =?", id).First(&payment).Error
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  200,
			"success": false,
			"message": "Data tidak ditemukan",
			"data":    payment,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"message": "Data ditemukan",
		"data":    payment,
	})
}

func InitHandler(c *gin.Context) {
	db := libs.DB()
	var paymentInput struct {
		Deposit     float64 `json:"deposit"`
		LastBalance float64 `json:"last_balance"`
		UserID      int     `json:"user_id"`
		UserName    string  `json:"username"`
	}

	err := c.ShouldBindJSON(&paymentInput)
	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("error on Field %s, condition : %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	var payments []models.Payment
	db.Where("user_id = ?", paymentInput.UserID).Last(&payments)

	if len(payments) > 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  200,
			"success": true,
			"message": "Sudah melakukan init",
			"data":    "",
		})
		return
	}
	percent, balance, remainingBalance := libs.Calculate(paymentInput.LastBalance, paymentInput.Deposit)

	db.Create((&models.Payment{BaseMoney: libs.Round(balance, -3), Percent: libs.Round(percent, -3), Deposit: libs.Round(paymentInput.Deposit, -3), RemainingBalance: libs.Round(remainingBalance, -3), Username: paymentInput.UserName, UserID: int64(paymentInput.UserID)}))
	db.Where("user_id = ?", paymentInput.UserID).Last(&payments)

	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"message": "Data disimpan",
		"data":    payments,
	})
}

func StoreHandler(c *gin.Context) {
	db := libs.DB()
	var paymentInput struct {
		Deposit     float64 `json:"deposit"`
		LastBalance float64 `json:"last_balance"`
		UserID      int     `json:"user_id"`
		UserName    string  `json:"username"`
	}

	err := c.ShouldBindJSON(&paymentInput)
	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("error on Field %s, condition : %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	var payment models.Payment
	err = db.Where("user_id = ?", paymentInput.UserID).Last(&payment).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  200,
			"success": true,
			"message": "init terlebih dulu",
			"data":    err.Error(),
		})
		return
	}

	percent, balance, remainingBalance := libs.Calculate(payment.RemainingBalance, paymentInput.Deposit)
	fmt.Printf("p %v b %v rb %v", percent, balance, remainingBalance)

	db.Create((&models.Payment{BaseMoney: libs.Round(balance, -3), Percent: libs.Round(percent, -3), Deposit: libs.Round(payment.Deposit, -3), RemainingBalance: libs.Round(remainingBalance, -3), Username: paymentInput.UserName, UserID: int64(paymentInput.UserID)}))
	c.JSON(http.StatusOK, gin.H{
		"status":  200,
		"success": true,
		"message": "Data disimpan",
		"data":    payment,
	})
}

func DeleteHandler(c *gin.Context) {
	db := libs.DB()
	id := c.Param("id")

	var payment models.Payment
	if err := db.Where("id =?", id).First(&payment).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  404,
			"success": false,
			"message": "Data tidak ditemukan",
			"data":    "",
		})
		return
	}

	if err := db.Delete(&payment, id).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  405,
			"success": false,
			"message": "Gagal Menghapus",
			"data":    err.Error(),
		})
	}
	c.JSON(http.StatusBadRequest, gin.H{
		"status":  405,
		"success": false,
		"message": "Data berhasil di hapus",
		"data":    payment,
	})
}
