FROM golang:1.18-alpine AS BUILDER
RUN apk add --no-cache gcc g++ git openssh-client
RUN apk add build-base
WORKDIR /app
COPY env.local .
COPY . .
RUN go mod download
RUN go mod verify
RUN GO111MODULE=on CGO_ENABLED=1 GOOS=linux GOARCH=amd64 GOPROXY=https://goproxy.cn,direct \
    go build -ldflags="-w -s" -o server

FROM alpine:3.14 as production
# Add certificates
RUN apk add --no-cache ca-certificates
# Copy built binary from builder
COPY --from=BUILDER /app/server .
COPY --from=BUILDER /app/env.local .
# Expose port
EXPOSE 8080
# Exec built binary
CMD ./server