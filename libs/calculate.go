package libs

func Calculate(lastBalance, deposit float64) (percent, balance, remainingBalance float64) {
	percent = (lastBalance * 1.75) / 100
	balance = deposit - percent
	remainingBalance = lastBalance - balance

	return percent, balance, remainingBalance
}
