package libs

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func DB() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("payment.db"), &gorm.Config{})
	if err != nil {
		panic("Failed Connect DB")
	}
	return db
}
