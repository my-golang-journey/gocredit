package libs

import "math"

func Round(n float64, decimals int32) float64 {
	return math.Round(n*math.Pow(10, float64(decimals))) / math.Pow(10, float64(decimals))
}
