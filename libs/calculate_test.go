package libs

import "testing"

type calculateTest struct {
	arg1, arg2, percent, balance, remainingBalance float64
}

var calcTests = []calculateTest{
	// {1000, 100, 5, 6, 7},
	{2000, 200, 35, 165, 1835},
	// {3000, 300, 20, 21, 22},
}

func TestCalculate(t *testing.T) {
	for _, test := range calcTests {
		percent, balance, remainingBalance := Calculate(test.arg1, test.arg2)
		if percent != test.percent {
			t.Errorf("percentOutput %v not equal to expected %v", percent, test.percent)
		}
		if balance != test.balance {
			t.Errorf("balanceOutput %v not equal to expected %v", balance, test.balance)
		}
		if remainingBalance != test.remainingBalance {
			t.Errorf("remainingBalanceOutput %v not equal to expected %v", remainingBalance, test.remainingBalance)
		}
	}
}

func TestCalculateSingle(t *testing.T) {
	gotPercent, gotBalance, gotRemainingBalance := Calculate(1000000.0, 100000.0)
	wantPercent := 17500
	wantBalance := 82500
	wantRemainingBalance := 917500

	if gotPercent != float64(wantPercent) {
		t.Errorf("Percent %v wanted %v", gotPercent, wantPercent)
	}
	if gotBalance != float64(wantBalance) {
		t.Errorf("Balance %v wanted %v", gotBalance, wantBalance)
	}
	if gotRemainingBalance != float64(wantRemainingBalance) {
		t.Errorf("Remaining Balance %v wanted %v", gotRemainingBalance, wantRemainingBalance)
	}
}

func BenchmarkAdd(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Calculate(4, 6)
	}
}
