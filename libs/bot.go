package libs

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"gocredit/models"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"github.com/leekchan/accounting"
)

func BOT() *tgbotapi.BotAPI {
	errEnv := godotenv.Load("env.local")
	if errEnv != nil {
		log.Fatalf("Some error occured. Err: %s", errEnv)
	}
	botToken := os.Getenv("BOT_TOKEN")
	bot, err := tgbotapi.NewBotAPI(botToken)
	if err != nil {
		log.Printf("Authorized on account %s", err)
	}
	return bot
}

func BotUpdate() {
	db := DB()
	bot := BOT()
	u := tgbotapi.NewUpdate(0)
	updates := bot.GetUpdatesChan(u)
	for update := range updates {
		// fmt.Printf("asd %v", update.Message.From.ID)
		if update.Message == nil { // ignore any non-Message updates
			continue
		}

		if !update.Message.IsCommand() { // ignore any non-command Messages
			continue
		}

		// Create a new MessageConfig. We don't have text yet,
		// so we leave it empty.
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

		// Split message to get Argument from user message
		message := strings.Split(update.Message.Text, " ")

		// remove slash in first message
		command := strings.ToLower(message[0][1:])

		var payment models.Payment
		// money format
		ac := accounting.Accounting{Symbol: "Rp. ", Precision: 2}
		// Check command message from user
		switch command {
		case "init":
			if len(message) < 3 {
				msg.Text = "Argumen tidak cukup \n"
				msg.Text += "ex: /init NominalBayar SisaHutang"
				break
			}
			err := db.Where("user_id = ?", update.Message.From.ID).Last(&payment).Error
			if err != nil {
				msg.Text = "Anda sudah melakukan init"
				// break
			}
			if payment.ID != 0 {
				msg.Text = "Anda sudah melakukan init"
				break
			}
			nominal, err := strconv.Atoi(message[1])
			if err != nil {
				fmt.Println("Unable convert String to Int")
			}
			lastBalance, err := strconv.Atoi(message[2])
			if err != nil {
				fmt.Println("Unable convert String to Int")
			}
			percent, balance, remainingBalance := Calculate(float64(lastBalance), float64(nominal))
			if percent < 0 {
				msg.Text = "Pembayaran Bunga Minus"
				break
			}
			s := fmt.Sprintf("%.f", balance)
			if balance < 0 {
				msg.Text = "Pembayaran Pokok Minus " + s
				break
			}
			if remainingBalance < 0 {
				msg.Text = "Pembayaran Sisa Hutang Minus "
				break
			}
			db.Create((&models.Payment{BaseMoney: Round(balance, -3), Percent: Round(percent, -3), Deposit: Round(float64(nominal), -3), RemainingBalance: Round(remainingBalance, -3), Username: update.Message.From.UserName, UserID: update.Message.From.ID}))
			msg.Text = "Pembayaran Sukses \n"
			msg.Text += "Sisa Hutang : " + ac.FormatMoney(remainingBalance) + " \n"
			msg.Text += "Bayar : " + ac.FormatMoney(nominal) + "\n"
			msg.Text += "Bunga : " + ac.FormatMoney(percent) + " \n"
			msg.Text += "Pokok : " + ac.FormatMoney(balance) + " \n"
		case "bayar":
			if len(message) < 2 {
				msg.Text = "Argument tidak cukup \n"
				msg.Text += "ex : /bayar NominalBayar"
				break
			}
			err := db.Where("user_id = ?", update.Message.From.ID).Last(&payment).Error
			if err != nil {
				msg.Text = "Silakan /init nominalBayar SisaHutang terlebih dulu"
				break
			}
			// db.Last(&payment)

			nominal, err := strconv.Atoi(message[1])
			if err != nil {
				fmt.Println("Unable convert String to Int")
			}
			percent, balance, remainingBalance := Calculate(payment.RemainingBalance, float64(nominal))
			if remainingBalance < 0 {
				reba := fmt.Sprintf("%.f", remainingBalance)
				msg.Text = "Pembayaran melebihi sisa Hutang" + reba
				break
			}
			db.Create((&models.Payment{BaseMoney: Round(balance, -3), Percent: Round(percent, -3), Deposit: Round(float64(nominal), -3), RemainingBalance: Round(remainingBalance, -3), Username: update.Message.From.UserName, UserID: update.Message.From.ID}))
			msg.Text = "Pembayaran Sukses \n"

			msg.Text += "Sisa Hutang Awal:" + ac.FormatMoney(payment.RemainingBalance) + " \n"
			msg.Text += "Sisa Hutang : " + ac.FormatMoney(remainingBalance) + " \n"
			msg.Text += "Bayar : " + ac.FormatMoney(nominal) + "\n"
			msg.Text += "Bunga : " + ac.FormatMoney(percent) + " \n"
			msg.Text += "Pokok : " + ac.FormatMoney(balance) + " \n"
		case "check":
			msg.Text = check(update)
		default:
			msg.Text = message[0]
		}

		if _, err := bot.Send(msg); err != nil {
			log.Printf("Authorized on account %s", err)
		}
	}
}

func check(update tgbotapi.Update) string {
	db := DB()
	ac := accounting.Accounting{Symbol: "Rp. ", Precision: 2}
	msg := ""
	start := time.Now()
	var payments []models.Payment
	// id, err := strconv.Atoi(message[1])
	// if err != nil {
	// 	fmt.Println("Unable convert String to Int")
	// }
	error := db.Where("user_id = ?", update.Message.From.ID).Find(&payments).Error
	elapsed := time.Since(start)

	if error != nil {
		msg = "Tidak ada Data\n" + "exec Time : " + elapsed.String()
		return msg
	}
	if len(payments) > 0 {
		msg = "Info Saldo \n"
	} else {
		msg = "Tidak ada Data \n" + "exec Time : " + elapsed.String()
		return msg
	}
	for i := 0; i < len(payments); i++ {
		t := payments[i].CreatedAt.Format("02 January 2006")
		msg += "Sisa Hutang : " + ac.FormatMoney(payments[i].RemainingBalance) + " \n"
		msg += "Bayar : " + ac.FormatMoney(payments[i].Deposit) + "\n"
		msg += "Bunga : " + ac.FormatMoney(payments[i].Percent) + " \n"
		msg += "Pokok : " + ac.FormatMoney(payments[i].BaseMoney) + " \n"
		msg += "Tanggal : " + t + " \n"
		msg += "------------------------------ \n"
	}
	countData := strconv.Itoa(len(payments))
	msg += "Loaded Data : " + countData + " \n"
	msg += "exec Time : " + elapsed.String() + "\n"
	msg += "reqBy : " + update.Message.From.UserName
	return msg
}
