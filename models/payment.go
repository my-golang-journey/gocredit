package models

import "gorm.io/gorm"

type Payment struct {
	gorm.Model
	ID               int
	BaseMoney        float64
	Percent          float64
	Deposit          float64
	RemainingBalance float64
	Username         string
	UserID           int64
}
