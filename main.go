package main

import (
	"fmt"
	"log"
	"regexp"
	"runtime"
	"time"

	"gocredit/libs"
	"gocredit/models"
	"gocredit/routers"
)

func main() {
	// defer TimeTrack(time.Now())
	routers.PaymentRouter()

	db := libs.DB()
	db.AutoMigrate(&models.Payment{})
	bot := libs.BOT()

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	libs.BotUpdate()
}

func TimeTrack(start time.Time) {
	elapsed := time.Since(start)

	// Skip this function, and fetch the PC and file for its parent.
	pc, _, _, _ := runtime.Caller(1)

	// Retrieve a function object this functions parent.
	funcObj := runtime.FuncForPC(pc)

	// Regex to extract just the function name (and not the module path).
	runtimeFunc := regexp.MustCompile(`^.*\.(.*)$`)
	name := runtimeFunc.ReplaceAllString(funcObj.Name(), "$1")

	log.Println(fmt.Sprintf("%s took %s", name, elapsed))
}
